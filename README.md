Tree/map of Major Graded Areas'

## https://jwsy.gitlab.io/markmap-mgas/

This is a viz for the MGAs using markmap (https://markmap.js.org/). 

Usage
-----
Update `mga.md` with updated MGAs in markdown format. Test your markdown with the `markmap-cli` command as shown below

```bash
# using node-lts currently at 16
npx -y markmap-cli --no-open -o public/index.html mga.md
```


---

Example plain HTML site using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.
