# MGAs

## 1 Managing Resources

### 1.1 Adequacy
- Manpower
- Funds
- Equipment
- Facilities
- Guidance

### 1.2 Stewardship
- Manpower
- Funds
- Equipment
- Facilities & Environment
- Guidance
- Airmen's Time

## 2 Leading People

### 2.1 Communication
- System
- Feedback
- Intent
- Comm-induced Waste
- Messaging
- Agility

### 2.2 Discipline
- Compliance
- Pride
- Accountability
- Customs, courtesies, and uniform wear
- Attention to detail

### 2.3 Training
- Individual
- Team
- Unit

### 2.4 Development
- Professional
  - PME
  - Mentorship
- Personal
  - Physical
  - Mental
  - Spiritual
  - Social

### 2.5 Quality of Life Engagement
- On-duty climate & morale
- Off-duty climate & morale
- Basic Services

## 3 Improving the Unit

### 3.1 Strategic Alignment
- Authorities
- Strategic Planning
- Performance Metrics

### 3.2 Process Operations
- Key Work Processes
- Risk Management
- Commitment to Continuous Improvement

### 3.3 Commander's Inspection Program (CCIP)
- Management
  - Self-Assessment Program
  - Wing IG Inspections
- Effectiveness
  - Accuracy
  - Adequacy
  - Relevance

### 3.4 Data-Driven Decisions
- Data Collection
- Decision Processes

## 4 Executing the Mission

### 4.1 Readiness
- Generate
  - Personnel/Equipment/Infrastructure/Security
  - C2/CDO
  - Performance/Readiness Reporting
- Employ
  - Command and Control
  - Performance
  - Contest, Degraded, and Operationally Limited
- Sustain
  - Personnel/Equipment/Infrastructure/Security
  - C2/CDO
  - Performance/COOP

### 4.2 Daily Operations
- Right Quality
- Right Quantity
- Right Time

### 4.3 Installation Preparedness
- Planning/Management
- Disaster Response

